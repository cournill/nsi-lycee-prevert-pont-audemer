---
author: Votre nom
title: Exercice avec plusieurs questions Python imbriquées
tags:
  - modulo
  - liste/tableau
---

???+ question "Exercice 1"

    
    **Question 1**
    
    Compléter la fonction `est_pair` qui prend en paramètre un nombre entier `nbre`, et renvoie `True` s'il est pair, `False` sinon.

    {{ IDE('pair') }}
    

    **Question 2**

    Compléter la fonction `filtre_pair` qui prend en paramètre un tableau de nombres entiers `entiers` et qui renvoie un nouveau tableau
    ne contenant que les nombres pairs de `entiers`.

    !!! danger "Contrainte"

        Vous appelerez **obligatoirement** la fonction écrite à la question 1. Elle est dans le code caché de cette question. Vous n'êtes pas obligé
        d'avoir réussi la question 1 pour répondre à cette question.

    {{ IDE('filtre') }}

